// Javascript Statements:
/*
>> set of instructions that we tell the computer/machine to perform
>>JS statements usually ends with semicolon (;), to locate where the statements end

*/ 

console.log("Hello once more");
// Comments - for readability
// one line comment (ctrl+/)
/*multi-line comment (ctrl + shift + /)*/

/*
>>Comments are parts of the code that gets ignored by the language
>>Comments are meant to describe the written code
*/

// Variables
	//used for containing data
/*
	Syntax:
		let / const variableName;
	*/

	// declaring variables

	//initialie variables
	let hello;      //initalization   - top to bottom ang reading
	console.log(hello); //result: undefined

	// console.log(x); //result:err
	// let x;

	// camel casing camilleDoroteo
	// for readability


	//Declaration Variables
	let firstName = "Jin"; //good variable name

	let pokemon = 25000; //bad variable name

	// di pwede basta gamitin yung caps letter sa una kasi andami nakaeserve na code sa java na nagstart sa caps

	//for class naming ang caps ang una

	let FirstName = "Izuku"; //bad variable name
	let firstNameAgain = "All Might"; //good variable name

	// let first name = "Midoriya"; //bad variable name

	//camelcase
		//lastName, emailAddress, mobileNumber
	//underscore

	let product_description ="lorem ipsum";
	let product_id = "250000ea1000";

	/*
		Syntax
			let / const variableName = value;
	*/

	let productName = "desktop computer"
	console.log(productName);

	// lumalabas yung value ng variable which is desktop computer

	let productPrice = 18999;
	console.log(productPrice); //results

	const pi = 3.14;
	console.log(pi); //result: 3.14

	/*Syntax:
		variableName = value
	*/

	productName = "Laptop";
	console.log(productName); //result: Laptop

	let friend = "Kate";
	friend = "Chance";
	console.log(friend);

	// let friend ="Jane";
	// console.log(friend); //result: err "identifier friend has already been declared"

	// pi = 3.1
	// console.log(pi); // result: err "assignment to constant variable"

	// use const unless it won't change

	//reassigning vs. initializing variable


	//initialize variable
	let supplier; //dineclare ko muna bago ko nilagyan ng value-initialize ang var
	supplier = "Jane Smith Tradings"
	console.log(supplier); //result: Jane Smith Tradings


	//reassignment
	supplier = "Zuitt Store";
	console.log(supplier); //result: Zuitt Store

	//var vs. let/const
	// var is to declare a variable

	a = 5 //setting value
	console.log(a);
	var a;


	// b = 6;
	// console.log(b); //result:err Cannot access 'b' before initialization
	// let b; 

	// var hoists, umaangat ang declaration let and const does not hoist

	//bumuo ng let and const, pwede mo palitan

	//let/const local/global scope

	// local scoping and global scoping

	let outerVariable ='hello';
	// kahit saan pwede ko tawagin

	/*{
		let innerVariable = 'hello again';
	}*/

	console.log(outerVariable); // result: hello
	// console.log(innerVariable); // result: innerVariable is not defined
	//sa curly braces mo lang pwde tawagin yung nasa curly braces
	

	//multiple Variable declaration
	let productCode = 'DC017', productBrand= 'Dell';
	console.log(productCode, productBrand); //result: DC017, Dell


	//let is a reserved keyword

	// const let = 'hello'
	// console.log(let); //result: err same with const

	//Data Types
	//String
	//series of characters

	let country = 'Philippines';
	let city = 'Manila City';

	console.log (city, country); //result: Manila City Philippines


	//Concatenating Strings

	let fullAddress = city + ',' + ' ' + country;
	console.log(fullAddress); //result: 

	let fullAddress1 = city + ', ' + country;
	console.log(fullAddress); //result: Manila Philipines

	let myName = 'Camille Doroteo';

	let greeting = 'Hi, I am ' + myName + '.';
	console.log(greeting); 

	//escape character (\)

	// "\n" creates a new line in between text

	let mailAddress ='Metro Manila\n\nPhilippines'
	console.log(mailAddress); //result line break between MM and PH

	let message = 'John\'s employees went home early';
	console.log(message); //prints out the message

	// let message1 = '\let me go';
	// console.log(message1);

	//Numbers
	//Integers/Whole Numbers

	let headcount = 26;
	console.log(headcount); // result: 26

	let grade = 74.9
	console.log(grade); //74.9

	//Exponential Notation

	let planetDistance = 2e10;
	console.log(planetDistance);

	console.log("John's grade last quarter is " + grade);

	console.log(message.length);

//Boolean

let isMarried = false;
let isSingle = true;
console.log('isMarried ' + isMarried);
console.log('isSingle ' + isSingle);

// pag concatinated nagiging strings

// Arrays
// it can store multiple values
/*
Syntax:
let/const arrayName = [];
*/

let grades = [98.7, 77.3, 90.8, 88.4];
console.log(grades);

const anime = ["BNHA", "AOT", "SxF", "KNY"];
console.log(anime);

//DIfferent Data Types
let random = ["JK", 24, true];
console.log(random);

//Object Data Type
/*
	Syntax:
		let/const objectName = {
	propertyA: valueA,
	propertyB: valueB
	}
*/

	let person = {
		fullName: "Edward Scissorhands",
		age: 35,
		isMarried: false,
		contact: ["09123456789", "8123 4567"],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	};

	console.log(person)

	// label si property
	//useful for abstract objects

	const myGrades = {
		firstGrading: 98.7,
		secondGrading: 77.3,
		thirdGrading: 88.6,
		fourthGrading: 74.9
	};

	console.log(myGrades)

	// mas kayang bigyan ng label ni object kasi may label
	// mas okay ang obj mas may sense

	// typeof operator
	console.log(typeof myGrades); //result: Object
	console.log(typeof grades); //result: Object (arrays are special type of object)
	console.log(typeof grade); //result: Number

	// methods, functions -- sa array

	// anime = ['One Punch Man'];
	// console.log(anime); //result: err (assignment to constant variable)

	anime[0]= ['One Punch Man'];
	console.log(anime);

	// 0 is index, nareplace si BNHA ng OPM
	//values can be changed even const is a constant variable sa array kase maraming data siya na hawak, pwde ka magdagdag

	//Null
	let spouse = null;
	console.log(spouse); //result - null, null is ibig sabihin ay wala, si null wala talaga, may dineclare na walang value, siya nabigyan ng value na null value na wala

	let zero=0; //zero meron pa rin value na zero
	let emprtString=' ';

	//undefined
	let y;
	console.log(y); //result: isa akong variable na walang value/data
	//placeholder yung walang declared value, or comparing data














